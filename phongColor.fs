#version 330

uniform sampler2D s_texture;
uniform float time;
in vec3 color;
in vec3 normal;

void main()
{
	vec3 lightDirection = normalize(vec3(1,1,1));
	vec3 viewDirection = vec3(0,0,1);
	float shininess = 100.0;

	float ambient = 0.2;
	float diffuse = 0.8 * dot(normalize(normal), lightDirection);

	vec3 r = reflect(-lightDirection, normalize(normal));

	float specular = pow(max(0.0, dot(r, viewDirection)), shininess);

	float factor = ambient + diffuse + specular;
	gl_FragColor = vec4(factor * 0.1, factor * 0.5, factor * 0.8, 1.0);
}