#define STB_IMAGE_IMPLEMENTATION
#include "Texture.h"
#include "stb_image.h"

Texture::Texture(std::string filename)
{
	int width, height, comp;
	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load(filename.c_str(), &width, &height, &comp, 4);
	if (!data)
	{
		printf("Failed to load: %s\n", stbi_failure_reason());
		throw "Failed to load";
	}

	// Generate an OpenGL texture ID for this texture
	glGenTextures(1, &textureId);
	// Bind to the new texture ID
	glBindTexture(GL_TEXTURE_2D, textureId);
	// Store the texture data for OpenGL use
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	for (int i = 3; i < (int)(width * height * 4); i += 4)
	{
		data[i] = 0.0f;
	}

	int totalPixels = width * height;
	float density = 0.8;

	int nrStrands = (int)(density * totalPixels);

	for (int i = 0; i < nrStrands; i++)
	{
		int x, y;

		// Random position on the texture
		x = rand() % width;
		y = rand() % height;

		// Fill with solid
		data[x * y * 4 + 3] = 255;
	}

	// Generate an OpenGL texture ID for this texture
	glGenTextures(1, &furTextureId);
	// Bind to the new texture ID
	glBindTexture(GL_TEXTURE_2D, furTextureId);
	// Store the texture data for OpenGL use
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	stbi_image_free(data);
}


Texture::~Texture(void)
{
	glDeleteTextures(1, &textureId);
}
