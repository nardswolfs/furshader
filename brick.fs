#version 330

uniform sampler2D s_texture;
uniform float time;
in vec3 color;
in vec3 normal;
in vec2 texCoord;

const int xcount = 20;
const int ycount = 35;

const float xsize = 1.0/xcount;
const float ysize = 1.0/ycount;

const float xspace = xsize / 14;
const float yspace = ysize / 12;

void main()
{
	vec3 lightDirection = normalize(vec3(1,1,1));
	vec3 viewDirection = vec3(0,0,1);
	float shininess = 100.0;

	float ambient = 0.2;
	float diffuse = 0.8 * dot(normalize(normal), lightDirection);

	vec3 r = reflect(-lightDirection, normalize(normal));

	float specular = pow(max(0.0, dot(r, viewDirection)), shininess);

	float factor = ambient + diffuse + specular;

	factor -= int(mod(texCoord.x + xsize / 2 * mod(int(texCoord.y * ycount), 2.0), xsize) < xspace || mod(texCoord.y, ysize) < yspace) * 0.8;

	gl_FragColor = vec4(factor * 0.9, factor * 0.3, factor * 0.1, 1);
}
