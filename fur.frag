#version 330

uniform sampler2D s_texture;
uniform sampler2D s_texture2;

in vertexData {
	vec3 normal;
	vec2 texCoord;
	float alpha;
	float strength;
} vertexIn;

float furContrast = 2.0f;
float furStrengthCap = 0.3;

void main()
{
	vec3 lightDirection = normalize(vec3(1,1,1));
	vec3 viewDirection = vec3(0,0,1);
	float shininess = 10.0;

	float ambient = 0.8;
	float diffuse = 0.2 * dot(normalize(vertexIn.normal), lightDirection);

	vec3 r = reflect(-lightDirection, normalize(vertexIn.normal));
	float specular = pow(max(0.0, dot(r, viewDirection)), shininess);

	vec4 furTexture;
	furTexture = mix(texture2D(s_texture2, vertexIn.texCoord), texture2D(s_texture, vertexIn.texCoord), step(0.95f, vertexIn.alpha));

	if(furTexture.a < 0.01f) discard;
	//float alpha = clamp(vertexIn.alpha * furTexture.r * furContrast - furStrengthCap, 0.0, 1.0);

	float factor = ambient + diffuse + specular;
	vec4 finalColor = vec4(furTexture.rgb * factor * vertexIn.strength, 1.0f);

	gl_FragColor = vec4(finalColor.rgb, vertexIn.alpha);
}