#version 330

layout (triangles) in;
layout (triangle_strip, max_vertices = 100) out;

in vertexData {
	vec3 normal;
	vec2 texCoord;
} vertexIn[];

out vertexData {
	vec3 normal;
	vec2 texCoord;
	float alpha;
	float strength;
} vertexOut;

uniform float distance;
uniform float time;

float furLength = distance / 8;
float maxFurLength = 1.5;
float currentLayer = 0.0f;
int numLayers = 10;

vec3 gravity = vec3(0, 0.5f, 0);

//float uvScale = 1.0f;

void main() 
{  
	// Set a max furLength and delta
	furLength = min(furLength, maxFurLength);
	float furDelta = furLength / numLayers;

	for (float i = 0.0f; i < numLayers; i += furLength / numLayers)
	{
		for (int i = 0; i < gl_in.length(); i++)
		{
			vertexOut.normal = vertexIn[i].normal;
			vertexOut.texCoord = vertexIn[i].texCoord;

			float k = pow(currentLayer, 3);
			gravity *= k;

			// The larger the layer number, the brighter and more transparent it becomes  
			vertexOut.alpha = 1.0f - currentLayer * 0.7f;
			vertexOut.strength = 1.0f - currentLayer * 0.7f;
			// Generate layers
			gl_Position = gl_in[i].gl_Position + vec4(vertexIn[i].normal * furLength * currentLayer, 1.0f) + vec4(gravity, 1.0f);

			EmitVertex();
		}
		// Increment layer
		currentLayer += furDelta;
		EndPrimitive();
	}
}  